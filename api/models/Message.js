/**
 * Created by jiacan on 2015-07-11.
 */

module.exports = {
  Message: {
    attributes: {
      name: 'string',
      email: 'email',
      message: 'string'
    }
  }
}
